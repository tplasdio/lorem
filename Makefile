VARS = \
	PRG='$(PRG)' \
	DESTDIR='$(DESTDIR)' \
	PREFIX='$(PREFIX)' \
	FORCEROOT='$(FORCEROOT)'

default: install

install:
	$(VARS) ./make.sh install

uninstall:
	$(VARS) ./make.sh uninstall

.PHONY: all default install uninstall
