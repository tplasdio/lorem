name=lorem-git
_name=lorem
__name=lorem  # how to name the app on installation, you may change this
desc="A simple CLI filler text generator"
version=.r25.c85b228
release=1
maintainer="tplasdio <tplasdio cat codeberg dog com>"
licenses=('BSD-3-Clause')
homepage='https://codeberg.org/tplasdio/lorem'
deps=(
 coreutils
)
deps_arch=(
 coreutils
 awk
)
deps_debian=(
 coreutils
 awk
)
deps_fedora=(
 coreutils
 # gawk
)
deps_alpine=(
 coreutils
 awk
)
build_deps=(
 sed
 make
)
architectures=('all')
sources=("git+$homepage")
checksums=('SKIP')

version() {
	cd "$srcdir/$_name"
	git-version
}

package() {
	cd "$srcdir/$_name"

	make install DESTDIR="$pkgdir" PREFIX=/usr PRG="$__name" FORCEROOT=1
}
