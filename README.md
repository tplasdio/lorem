<p align="center">
  <h1 align="center">Lorem</h1>
  <p align="center">
    A minimal filler text generator, written in shell script
  </p>
</p>

## 📰 Description

Generate filler text with your shell. Simple.
No internet needed. No crazy dependency or database.
No compilation. Not a library.

## 🚀 Installation

<details>
<summary>📦 From source</summary>

- *Dependencies*: `sh`, `awk`, `paste`, `tr`, `mkfifo` and `shuf` from GNU's coreutils
- *Build Dependencies*: `sed`, `git`

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/lorem.git
cd lorem
```

For user-installation, either
```sh
./make.sh install  # or
# make install
```

Run as root for system-installation.

For uninstallation, run either `./make.sh uninstall` or `make uninstall`.

</details>

<details>
<summary>Arch Linux</summary>

```sh
curl -O https://codeberg.org/tplasdio/lorem/raw/branch/main/packaging/PKGBUILD-git
makepkg -sip PKGBUILD-git
```

*Note: The package and executable are named `shlorem` in Arch to not conflict with AUR's `lorem`,
you may modify the PKGBUILD before installing*

For uninstallation, run `sudo pacman -Rsn shlorem-git`.

</details>

<details>
<summary>Other Linux</summary>

Install [lure](https://gitea.elara.ws/Elara6331/lure/releases/latest)

```sh
curl -O https://codeberg.org/tplasdio/lorem/raw/branch/main/packaging/lure-git.sh
lure build -s lure-git.sh
```

- Debian, Ubuntu or other .deb distros: `dpkg -i lorem-git*.deb`
- Fedora, OpenSUSE or other .rpm distros: `rpm -i lorem-git*.rpm`
- Alpine Linux: `apk add --allow-untrusted lorem-git*.apk`

</details>

## ⭐ Usage

Words are used by default from the `${XDG_DATA_HOME}/lorem` text file, defaulting
to `~/.local/share/lorem`, falling back to `/usr/share/lorem/lorem`.

```sh
lorem                # Print 60 random latin words, punctuations every 5 words
lorem 2000           # Print 2000 random latin words
lorem -w 2000        # Same as ↑
lorem --wpp 3        # With punctuations every 3 words
lorem -l             # Always printing "Lorem ipsum" at the start
lorem -d '. !'       # Only with certain punctuations
lorem | fold -sw 40  # Pipe to fold to split lines with a maximum width
SHUF=gshuf lorem     # For macOS or some Unix systems with other name for `shuf`
lorem - < file       # Print from words read from file lines

# Generating filler text with specific words:
printf "%s\n" one morning when Gregor Samsa \
  woke from troubled dreams he found himself \
  transformed in his bed into a horrible vermin \
  | lorem - 200 | fold -sw50
```

## 👀 See also

- [Awesome-ipsum](https://github.com/templeman/awesome-ipsum): List of other lorem generators.
- [Lipsum](https://github.com/hannenz/lipsum): Another good CLI lorem generator, has a GUI.
- [Lorem](https://gitlab.gnome.org/World/design/lorem): A GUI for Gnome desktop.

## 📝 License

BSD-3clause
