#!/bin/sh

setup() {

	: \
	"${DESTDIR:="/"}" \
	"${PRG:=lorem}"

	PRG_PATH=lorem
	DB=lorem
	DB_PATH="share/lorem"
	LICENSE="LICENSE"
	XDG_PREFIX="${HOME}/.local"

	[ "$(id -u)" -eq 0 ] || [ "$FORCEROOT" = 1 ] \
		&& isroot=: \
		|| isroot=false

	$isroot \
		&& prefix="${PREFIX:=/usr/local}" \
		|| prefix="${XDG_PREFIX}"

	datadir="$prefix/share" \
	bindir="$prefix/bin" \
	localedir="$prefix/share/locale" \
	mandir="$prefix/share/man" \
	licensedir="$prefix/share/licenses/$PRG"
}

install()
{
	# for user installation, database will be a simple file,
	# for system installation it'll be inside a directory

	$isroot \
		&& DB_TARGET_PATH="${datadir}/${PRG}/${DB}" \
		|| DB_TARGET_PATH="${datadir}/${DB}"

	mkdir -p -- "${DESTDIR}/${bindir}" "${DESTDIR}/${licensedir}" "${DESTDIR}/${datadir}"

	$isroot \
		&& sed '/@make-systemdb/s|/usr/share/lorem/lorem|'"${DB_TARGET_PATH}"'|' "${PRG_PATH}" > "${PRG_PATH}.bak" \
		|| cp -- "${PRG_PATH}" "${PRG_PATH}.bak"

	$isroot \
		&& mkdir -p -- "${DESTDIR}/${datadir}/${PRG}" \
		|| touch -- "${DESTDIR}/${DB_TARGET_PATH}"

	chmod 755 -- "${PRG_PATH}.bak"
	chmod 644 -- share/*
	cp -- "${PRG_PATH}.bak" "${DESTDIR}/${bindir}/${PRG}"
	cp -- "${DB_PATH}" "${DESTDIR}/${DB_TARGET_PATH}"
	cp -- "$LICENSE" "${DESTDIR}/${licensedir}/LICENSE"

	rm -f -- "${PRG_PATH}.bak"
}

uninstall()
{
	rm -f -- "${DESTDIR}/${bindir}/${PRG}"
	$isroot \
		&& rm -rf --  "${DESTDIR}/${datadir}/${PRG}" \
		|| rm -f -- "${DESTDIR}/${datadir}/${DB}"
	rm -rf -- "${DESTDIR}/${licensedir}"
}

main()
{
	set -x
	# set -e
	setup
	target="${1-install}"

	case "$target" in
	(i|install)   install;;
	(u|uninstall) uninstall;;
	(*)
		printf -- 'Unknown target given: "%s"\n' "$1"
		return 22
	esac
}

main "$@" || exit $?
